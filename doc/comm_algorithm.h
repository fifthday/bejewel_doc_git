// 2011-11-15
// comm_algorithm.h
// guosh
// 项目中用到的算法

#ifndef _COMM_ALGORITHM_H_
#define _COMM_ALGORITHM_H_

#include "xcore_define.h"

namespace comm {

////////////////////////////////////////////////////////////////////////////////
// class Algorithm
////////////////////////////////////////////////////////////////////////////////
class Algorithm
{
public:
	static string make_authkey(const string& passwd);

	static bool check_authkey(const string& passwd, const string& authkey);
};

}//namespace comm

using namespace comm;

#endif//_COMM_ALGORITHM_H_
