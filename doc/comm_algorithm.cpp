// 2011-11-15
// comm_algorithm.cpp
// guosh
// 项目中用到的算法


#include "comm_algorithm.h"
#include "xcore_md5.h"
#include "xcore_str_util.h"


namespace comm {
	
////////////////////////////////////////////////////////////////////////////////
// class Algorithm
////////////////////////////////////////////////////////////////////////////////
string Algorithm::make_authkey(const string& passwd)
{
	static string str = "0123456789ABCDEF";
	srand((uint32)time(NULL));
	string str1 = xcore::md5(passwd.c_str(), passwd.size());
	char ch1 = str.at(rand() % 16);
	char ch2 = str.at(rand() % 16);
	str1.at(0) = ch1;
	str1.at(31) = ch2;
	string str2 = xcore::md5(str1.c_str(), str1.size());
	str2.at(0) = ch1;
	str2.at(31) = ch2;
	XStrUtil::to_upper(str2);
	return str2;
}

bool Algorithm::check_authkey(const string& passwd, const string& authkey)
{
	if (authkey.size() != 32) return false;
	char ch1 = authkey.at(0);
	char ch2 = authkey.at(31);
	string str1 = xcore::md5(passwd.c_str(), passwd.size());
	str1.at(0) = ch1;
	str1.at(31) = ch2;
	string str2 = xcore::md5(str1.c_str(), str1.size());
	str2.at(0) = ch1;
	str2.at(31) = ch2;
	XStrUtil::to_upper(str2);
	return (str2 == authkey);
}

}//namespace comm
